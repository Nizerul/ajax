var submitURLencoded = document.querySelector(".urlencoded");
var submitJSON = document.querySelector(".json");
var preloader = document.querySelector(".preloader");
var error = document.querySelector(".error");
var loginForm = document.querySelector(".login-form");
var logout = document.querySelector(".logout-btn");

var personData = document.querySelector(".person-data");
var avatar = document.querySelector(".avatar");
var fullName = document.querySelector(".fullName");
var country = document.querySelector(".country");
var hobbies = document.querySelector(".hobbies");

submitURLencoded.addEventListener("click", function(e){
    e.preventDefault();
    var xhr = new XMLHttpRequest();
    var email = document.querySelector("#email").value;
    var password = document.querySelector("#password").value;
    var body = "email=" + encodeURIComponent(email) + "&password=" + encodeURIComponent(password);
    
    xhr.open("POST", "http://netology-hj-ajax.herokuapp.com/homework/login_json", true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    
    xhr.addEventListener("loadstart", function(){
       preloader.style.display = "block";
       loginForm.style.display = "none";
    });
    
    xhr.addEventListener("loadend", function(){
       preloader.style.display = "none";
    });
    
    xhr.addEventListener("load", function(){
        if (xhr.status >= 200 && xhr.status < 300){
            var person = JSON.parse(xhr.responseText);
            
            personData.style.display = "block";
            logout.style.display = "block";
            
            avatar.src = person.userpic;
            fullName.innerHTML = "Имя: " + person.name + ' ' + person.lastname;
            country.innerHTML = "Страна: " + person.country;
            hobbies.innerHTML = "Хобби: " + person.hobbies;
        }else{
            loginForm.style.display = "block";
            error.style.display = "block";
            setTimeout(function(){
                error.style.display = "none";
            }, 3000);
        }
    })
    
    xhr.send(body);
});

submitJSON.addEventListener("click", function(e){
    e.preventDefault();
    var xhr = new XMLHttpRequest();
    var email = document.querySelector("#email").value;
    var password = document.querySelector("#password").value;
    var body = JSON.stringify({
        email: email,
        password: password
    });
    
    xhr.open("POST", "http://netology-hj-ajax.herokuapp.com/homework/login_xml", true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    
    xhr.addEventListener("loadstart", function(){
       preloader.style.display = "block";
       loginForm.style.display = "none";
    });
    
    xhr.addEventListener("loadend", function(){
       preloader.style.display = "none";
    });
    
    xhr.addEventListener("load", function(){
        if (xhr.status >= 200 && xhr.status < 300){
            var person = new DOMParser().parseFromString(xhr.responseText, "text/xml");
            
            personData.style.display = "block";
            logout.style.display = "block";
            
            avatar.src = person.getElementsByTagName("userpic")[0].childNodes[0].nodeValue;
            fullName.innerHTML = "Имя: " + person.getElementsByTagName("name")[0].childNodes[0].nodeValue + ' ' + person.getElementsByTagName("lastname")[0].childNodes[0].nodeValue;
            country.innerHTML = "Страна: " + person.getElementsByTagName("country")[0].childNodes[0].nodeValue;
            hobbies.innerHTML = "Хобби: " + person.getElementsByTagName("hobby")[0].childNodes[0].nodeValue + ", " + person.getElementsByTagName("hobby")[1].childNodes[0].nodeValue + ", " + person.getElementsByTagName("hobby")[2].childNodes[0].nodeValue + ", " + person.getElementsByTagName("hobby")[3].childNodes[0].nodeValue;
        }else{
            loginForm.style.display = "block";
            error.style.display = "block";
            setTimeout(function(){
                error.style.display = "none";
            }, 3000);
        }
    })
    
    xhr.send(body);
});

logout.addEventListener("click", function(){
    loginForm.style.display = "block";
    personData.style.display = "none";
    logout.style.display = "none";
    avatar.src = "";
    fullName.innerHTML = "";
    country.innerHTML = "";
})